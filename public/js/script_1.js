import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyBylojcWCoIoW7sxJLFgqn0E1QWnHe1mKY",
    authDomain: "controlacceso-ad2e6.firebaseapp.com",
    projectId: "controlacceso-ad2e6",
    storageBucket: "controlacceso-ad2e6.appspot.com",
    messagingSenderId: "512794588376",
    appId: "1:512794588376:web:5cc7949f16c0c89b5893ad"
};

const app = initializeApp(firebaseConfig);
console.log("consola de pruebas...");

let correoRef = document.getElementById("exampleInputEmail1");
//console.log(correoRef);

let passRef = document.getElementById("exampleInputPassword1");
//console.log(passRef);

let buttonCrearRef = document.getElementById("crearButtonId");
let buttonIngreasrRef = document.getElementById("ingresarButtonId");
//console.log(buttonRef);

buttonCrearRef.addEventListener("click", crearUsuario);
buttonIngreasrRef.addEventListener("click", chequeoUsuario);
const auth = getAuth();


function crearUsuario() {
    createUserWithEmailAndPassword(auth, correoRef.value, passRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Usuario creado");
            correoRef.value = '';
            passRef.value = '';
            window.location.href = "./foro.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
            // ..
            console.log("incorrecto");
        });
    
}
function chequeoUsuario() {

    if ((correoRef.value != '') && (passRef.value != '')) {

        signInWithEmailAndPassword(auth, correoRef.value, passRef.value)
            .then((userCredential) => {
                // Signed in
                const user = userCredential.user;
                window.location.href = "./foro.html";
                // ...
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
            });
    }
    else {
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }



}

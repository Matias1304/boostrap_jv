console.log("Consola de pruebas...");

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

const firebaseConfig = {
    apiKey: "AIzaSyBylojcWCoIoW7sxJLFgqn0E1QWnHe1mKY",
    authDomain: "controlacceso-ad2e6.firebaseapp.com",
    databaseURL: "https://controlacceso-ad2e6-default-rtdb.firebaseio.com",
    projectId: "controlacceso-ad2e6",
    storageBucket: "controlacceso-ad2e6.appspot.com",
    messagingSenderId: "512794588376",
    appId: "1:512794588376:web:5cc7949f16c0c89b5893ad"
};

const app = initializeApp(firebaseConfig);



const database = getDatabase(app);

let textoRef = document.getElementById("textoBaseDatosId");
let botonRef = document.getElementById("buttonBaseDatosId");
let valorDataBaseRef = document.getElementById("valorDataBaseId");

botonRef.addEventListener("click", cargarInformacion);

function cargarInformacion(){
    let informacion = textoRef.value;

    set(ref(database, 'datos/'), {
        data: informacion
      });

    console.log("Carga de información correcta.");
}

onValue(ref(database, 'datos/'), (snapshot) => {
    const lectura = snapshot.val();

    console.log(lectura.data);

    valorDataBaseRef.innerHTML = lectura.data;
});
